import React from 'react';

export default function FileUpload({ input, btnTitle }) {
    const { value, ...inputProps } = input;

    const handleChange = e => {
        const file = e.target.files[0];
        input.onChange(file);
    };

    return (
        <div className="file-field input-field">
            <div className="btn">
                <span>{btnTitle || 'File'}</span>
                <input {...inputProps} type="file" onChange={handleChange} />
            </div>
            <div className="file-path-wrapper">
                <input className="file-path validate" type="text" />
            </div>
        </div>
    );
}
