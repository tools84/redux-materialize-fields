export default function InputError({meta}){
    const {error, warning, touched } = meta;
    if(!error || !warning) return false;

    return (<div className="form-field-error">
        {error && <span className="field-error">{error}</span>}
        {warning && <span className="field-warn">{warning}</span>}
    </div>)
}