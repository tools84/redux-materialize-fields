import React from 'react';

const Switch = ({ input, label='', className, style = {} }) => {
    const checked = input.value == true ? 'checked' : '';

    return (
        <div className={`input-field ${className}`} style={style}>
            <div className="switch">
                <label>
                    {label}
                    <input {...input}  type="checkbox" checked={checked} />
                    <span className="lever" />
                </label>
            </div>
        </div>
    );
};

Switch.defaultProps = {
    label: '',
    className: '',
};

export default Switch;