import * as React from 'react';
import InputError from '../_common/InputError';

const Checkbox = ({ input, label, isRequired, onClick, meta }) => {
    const checked = input.value ? 'checked' : '';

    return (
        <div className={`input-field checkbox`}>
            <div className={isRequired ? 'required' : ''}>
                <label>
                    <input {...input} onClick={onClick} type="checkbox" checked={checked} />
                    {label && <span>{label}</span>}
                </label>
                <InputError meta={meta} />
            </div>
        </div>
    );
};

export default Checkbox;
