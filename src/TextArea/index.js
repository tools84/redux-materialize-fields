import React, { useRef, useEffect } from 'react';
import InputError from '../_common/InputError';

const TextArea = ({ input, label, placeholder = '', className, isRequired, meta }) => {
    const textareaRef = useRef();

    useEffect(() => {
        M.textareaAutoResize(textareaRef.current);
    }, []);

    return (
        <div className={`input-field ${className} ${isRequired ? 'required' : ''} `}>
            <label className="active">{label}</label>

            <InputError meta={meta} />
            <textarea ref={textareaRef} className="materialize-textarea" {...input} rows="10" placeholder={placeholder} />
        </div>
    );
};

export default TextArea;
