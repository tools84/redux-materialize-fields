import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

const Autocomplete = ({ input, data, icon, defaultValue, placeholder, onSelectCallback, meta }) => {
    const [typingTimeout, setTypingTimeout] = useState();
    const elRef = useRef();
    const instance = useRef();

    useEffect(() => {
        const options = {
            data,
            onAutocomplete: val => {
                onSelectEvent(val);
            }
        };

        instance.current = window.M.Autocomplete.init(elRef.current, options);

        return () => {
            instance.current.destroy();
        };
    }, []);

    // componentDidUpdate(prevProps, prevState){
    //     const { data, input, onKeywordChangeCallback } = this.props;
    //
    //     if(prevProps.input.value !== input.value) {
    //         console.error('__ Autocomplete::input val changed');
    //         this.instance.open();
    //         onKeywordChangeCallback();
    //     }
    //
    //     if(prevProps.data !== data){
    //         console.error('__ Autocomplete:: data changed', {oldData:prevProps.data, data});
    //         this.instance.updateData( data );
    //         // this.instance.open();
    //     }
    // }

    const valueChange = e => {
        const keyword = e.target.value;
        if (typingTimeout) clearTimeout(typingTimeout);

        // setTypingTimeout({
        //     typingTimeout: setTimeout(async() => {
        //         console.warn("**** Autocomplete CHANGED _____", keyword);
        //         input.onChange(keyword);
        //         instance.current.open();
        //     }, 800)
        // });
    };

    const onSelectEvent = newValue => {
        console.log('onSelectEvent', newValue);
        if (!newValue) return;

        input.onChange(newValue);
        if (onSelectCallback) onSelectCallback(newValue);

        // At the end of all state changes
        instance.current.close();
    };

    console.log('Autocomplete::render', { input, data });

    return (
        <div className="input-field">
            {/*<i className="material-icons prefix">{icon}</i>*/}

            <input
                ref={elRef}
                type="text"
                id="autocomplete-input"
                className="autocomplete"
                onChange={valueChange}
                placeholder={placeholder}
                defaultValue={defaultValue}
                autoComplete="off"
            />
        </div>
    );
};

Autocomplete.propTypes = {
    data: PropTypes.object.isRequired
};

export default Autocomplete;
