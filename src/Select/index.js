import React, { Fragment, useRef, useEffect } from 'react';


const Select = ({ input, id = 1, label, options=[], disabled, className = '', onChange, children }) => {
    const elRef = useRef();
    let instance = useRef();

    useEffect(() => {

         instance.current = window.M.FormSelect.init(elRef.current, {
			dropdownOptions: {
				coverTrigger: false,
                forSelect: true
            }
		});
        // window.M.AutoInit();

        return () => {
            instance.current && instance.current.destroy();
        };
    }, [options.length, input?.value]);


    const onChangeEvent = val => {
        if (onChange) onChange(val, id);
        input.onChange(val);
    };

    // console.log('init  SELECT', {input });

    return (
        <div className={`input-field selectField ${className}`}>
            <select id={id} ref={elRef} disabled={disabled} onChange={e => onChangeEvent(e.target.value)} value={input.value}>
                <option value="">---</option>
                {options?.map((item, i) => (
                    <option key={i} value={item.value}>
                        {item.name}
                    </option>
                ))}
                {/*{!options && children && <Fragment>{children}</Fragment>}*/}
            </select>

            <label>{label}</label>
        </div>
    );
};

Select.defaultProps = {
    id: 1,
    label: '',
    defaultValue: '',
    options: [],
    disabled: false,
    onChange: false
};

export default Select;
