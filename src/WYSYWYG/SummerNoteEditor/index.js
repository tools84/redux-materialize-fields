//https://demo.opencart.com/admin/view/javascript/summernote/opencart.js
//https://github.com/summernote/react-summernote/blob/master/src/Summernote.jsx
//https://summernote.org/getting-started/#basic-api

import React, { useRef, useState, useEffect} from 'react';
import axios from "axios";


export default function SummerNoteEditor({input, label }){
    const editor= useRef();
    const [mounted, setMounted] = useState(false);
    const [valInitialized, setValInitialized] = useState(false);
console.log('-----SUMMER EDITOR', {input});




    useEffect(()=>{
        if(mounted && !valInitialized && input.value.length>0){
            window.$(editor.current).summernote('code', input.value);
            setValInitialized(true);
        }
    },[mounted, input.value]);


    useEffect(()=>{
        if(mounted){
            window.$(editor.current).summernote({
                height: 150,
                tabsize: 2,
                placeholder: 'Hello stand alone ui',
                disableDragAndDrop: true,
                fontsize: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '30', '36', '48' , '64'],
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link',   'video','picture']],
                    ['view', ['fullscreen', 'codeview', 'help']],
                    // ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']]
                ],
                callbacks: {
                    // onFileUpload: function (files) {},

                    onChange: function(val, $editable) {
                        console.log('%c onChange', 'color:blue', {val,});
                        // input.onChange(val);
                    },
                    onBlur: function(val) {
                        console.log('%c onBlur', 'color:blue', val);
                        input.onChange(val); //try to update redux 1 time
                    },
                    onImageUpload:  (files)=> {
                        const file = files[0];
                        const data = new FormData();
                        data.append('file', file);

                        console.log('%c onImageUpload', 'color:blue', file);

                        axios.post('/api/fs?folder=summernote', data, {
                            headers: {'Content-Type': 'multipart/form-data'}
                        })
                            .then(({data}) => {
                                console.log('%c UPLOADED', 'color:blue', data);
                                window.$('#summernote').summernote('insertImage', data.path);
                            })
                            .catch(err => {
                                console.log('%c catch', 'color:red', err);
                            });
                    },
                }
            });
            setMounted(true);
        }else{
             setMounted(true);
        }
    },[mounted]);


    return (<div>
        {label && <label>{label}</label>}
        <textarea ref={editor} name='text' id="summernote" ></textarea>
    </div>)
}