import React, { useRef, useState, useEffect} from 'react';
// import ImageResize from '@ckeditor/ckeditor5-image/src/imageresize';

export default function MyEditor({input,  meta } ){
	const editorRef = useRef()
	const [ editorLoaded, setEditorLoaded ] = useState( false )
	const { CKEditor, ClassicEditor} = editorRef.current || {}
console.log('---CKEditor', {CKEditor,  ClassicEditor});
	useEffect( () => {
		editorRef.current = {
			CKEditor: require( '@ckeditor/ckeditor5-react' ).CKEditor, //Added .CKEditor
			ClassicEditor: require( '@ckeditor/ckeditor5-build-classic' ),
			// ImageResize: require( '@ckeditor/ckeditor5-image/src/imageresize' ),
			// CKFinder: require( '@ckeditor/ckeditor5-ckfinder/src/ckfinder' ),
		}
		setEditorLoaded( true );

		//Add script
		const script = document.createElement('script');
		script.src = `https://ckeditor.com/apps/ckfinder/3.5.0/ckfinder.js`;
		script.async = true;
		document.body.appendChild(script);
		return () => {
			document.body.removeChild(script)
		}
	}, []);



	return(<>
			{editorLoaded ? <CKEditor
				editor={ ClassicEditor }
				data={input.value}
				onReady={ editor => {
					// You can store the "editor" and use when it is needed.
					console.log('Editor is ready to use!', editor);
					console.log('---TOOLBARS', Array.from( editor.ui.componentFactory.names() ))
				} }
				onChange={ (event, editor ) => {
					// const toolbars=Array.from( editor.ui.componentFactory.names() );

					const data = editor.getData()
			 		input.onChange(data);
				}}
				config={{
					// plugins:[],
					toolbar: ['ckfinder','heading', '|', 'bold', 'italic', 'underline', 'strikethrough', '|', 'fontSize', 'fontColor', 'fontBackgroundColor', '|', 'alignment', 'outdent', 'indent', 'bulletedList', 'numberedList', 'blockQuote', '|', 'link', 'insertTable', 'imageUpload', 'mediaEmbed', '|', 'undo', 'redo'],
					autoParagraph: false,
					mediaEmbed: { previewsInData: true }, //Display Youtube videos
					ckfinder: {
						uploadUrl: 'http://localhost:3000/api/ckFinder',
						options: {
							connectorPath: '/api/ckFinder/',
							imageExtenstions: ['jpg', 'jpeg', 'svg', '.svg', 'svg+xml', 'png'],
							allowedExtensions: ['jpg', 'jpeg', 'svg', '.svg', 'svg+xml', 'png']
							// resourceType: 'Images',
						}
					},
					image: {
						resizeUnit: 'px',
						// toolbar: [
						// 	'imageTextAlternative',
						// 	'|',
						// 	'imageStyle:alignLeft',
						// 	'imageStyle:alignCenter',
						// 	'imageStyle:full',
						// 	'imageStyle:alignRight',
						// 	'imageStyle:fullWidth',
						// 	'imageStyle:customAlign'
						// ],
						// styles: [
						// 	'full',
						// 	'side',
						// 	'alignLeft',
						// 	'alignCenter',
						// 	'alignRight',
						// 	// { name: 'fullWidth', icon: 'full', title: 'Full layout width', className: 'image-full-layout-width' },
						// 	// { name: 'customAlign', icon: 'left', title: 'Custom: Full layout, align left', className: 'image-full-layout-align-left'},
						// 	{ name: 'customAlign', icon: 'left', title: 'Custom: Full layout, align left',  },
						// 	// { name: 'center-thumb-200', icon: 'center', title: 'Center thumb 200', className: 'image-style-align-center thumb-200' },
						// ],


						styles: {
							// options: [
							// 	{
							// 		name: 'margin-left',
							// 		// icon: 'left',
							// 		title: 'Image on left margin',
							// 		className: 'image-margin-left',
							// 		modelElements: [ 'imageInline' ]
							// 	}
							// ],
							// toolbar: [
							// 	'imageTextAlternative',
							// 	'toggleImageCaption',
							// 	'imageStyle:alignLeft',
							// 	"imageStyle:wrapText",
							// 	'alignLeft',
							// 	'|',
							// ]
						},

						upload: {
							types: ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'svg+xml', '.svg', 'svg']
						},
						imageExtenstions: ['jpg', 'jpeg', 'svg', '.svg', 'svg+xml', 'png'],
						allowedExtensions: ['jpg', 'jpeg', 'svg', '.svg', 'svg+xml', 'png']
					},
				}}
			/> : <p>Loading Editor...</p>}
		</>
	)
}
