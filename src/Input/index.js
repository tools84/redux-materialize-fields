import React from 'react';
import InputError from "../_common/InputError";

const Input = ({ input, type='text', label='', placeholder='', disabled=false, meta }) => {
    return (
        <div className="input-field">
            <label className="active" htmlFor={input?.name}>{label}</label>
			<input {...input} type={type} placeholder={placeholder} disabled={disabled} autoComplete="off" />

            <InputError meta={meta}/>
        </div>
    );
};

Input.defaultProps = {
    type: 'text',
    value: 'New York',
    placeholder: 'City',
    className: 'validate'
};

export default Input;
