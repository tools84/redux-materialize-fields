# Redux Materialize Fields

## Installation
```
npm install https://gitlab.com/tools84/redux-materialize-fields.git

npm install git@gitlab.com:tools84/redux-materialize-fields.git
npm i -S git+ssh://git@gitlab.com:tools84/redux-materialize-fields.git


npm i -S git+ssh://git@gitlab.com:tools84/redux-materialize-fields.git#develop


```


### Setup Materialize styles/js
```javascript
//in index.html
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
```


## Components
- Input
- Select
- Checkbox


