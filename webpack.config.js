const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = (env, args)=>{
	const { mode } = args;
	console.log('WBBBB', {env, args});

	return {
		mode: mode,
		entry: './src',
		// styles: './src/styles.ts'
		output: {
			path: path.resolve('lib'),
			filename: 'index.js',
			globalObject: 'this',
			// libraryTarget: 'commonjs2'
			// libraryTarget: 'umd',
			libraryTarget: 'commonjs-module',
			clean: true,
		},
		plugins: [new MiniCssExtractPlugin()],
		resolve: {
			extensions: ['.ts', '.tsx', '.js', '.jsx'],
			alias: {
				react: path.resolve(path.join(__dirname, './node_modules/react')),
			}
		},
		module: {
			rules: [
				{
					test: /\.js?$/,
					exclude: /node_modules/,
					use: { loader:'babel-loader'},
					// options: {
					// 	presets: ['react']
					// }
				},
				{
					test: /\.css$/i,
					use: [MiniCssExtractPlugin.loader, 'css-loader']
				}
			]
		},
		externals: {
			react: 'react',
		},
	}
};
